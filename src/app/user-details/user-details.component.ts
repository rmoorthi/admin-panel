import { UserService } from '@app/_services';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html'
})
export class UserDetailsComponent implements OnInit {
  data: any;
  id: number;
  constructor(public user: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
   this.id = this.route.snapshot.params.id;
   this.user.getSingleUser(this.id).subscribe(users => {
    this.data = users.data;
});
  }

}
