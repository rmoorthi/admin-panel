import { User } from '@app/_models';
export interface Search {
    total: number;
    data : User[];
}