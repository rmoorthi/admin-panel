﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Search } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll(pageNumber) {
        return this.http.get<any>(`${environment.apiUrl}/users?page=${pageNumber}`);
    }
    getSingleUser(id) {
        return this.http.get<any>(`${environment.apiUrl}/users/${id}`)
    }
}