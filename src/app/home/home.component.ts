﻿import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';
import { ActivatedRoute } from '@angular/router';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    pager = {};
    users = [];
    loading = false;
    constructor(
        private http: HttpClient,
        private route: ActivatedRoute,
        private user: UserService
    ) { }

    ngOnInit() {
        // load page based on 'page' query param or default to 1
        this.loading = true;
        this.route.queryParams.subscribe(x => this.loadPage(x.page || 1));
    }

    private loadPage(page) {
        // get page of items from api
        this.user.getAll(page).subscribe(x => {
            this.loading = false;
            this.pager = {
                totalItems: x.total,
                currentPage: x.page,
                pageSize: x.per_page,
                pages: Array(x.total_pages),
                maxPages: x.total_pages
            };
            this.users = x.data;
        
        });
    }
}